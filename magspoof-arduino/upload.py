import os
import sys
from cStringIO import StringIO
from intelhex import IntelHex

PROGRAMMER = 'usbtiny'
AVRDUDE_PARAMS = r''

ORIG_BIN = 'magspoof-arduino.ino.hex'
NEW_BIN = 'magspoof-arduino.ino.hex.patched'
ORIG_T1 = '%B123456781234567^LASTNAME/FIRST^YYMMSSSDDDDDDDDDDDDDDDDDDDDDDDDD?'
ORIG_T2 = ';123456781234567=YYMMSSSDDDDDDDDDDDDDD?'

ORIG_T1_OFFSET = 0x06C4 + 9 #1006C400  
ORIG_T2_OFFSET = 0x06A4 #1006A400

def main():
    phone_num = raw_input('Phone [039723333]: ') or '039723333'
    padded_phone_num = phone_num.ljust(len('123456781234567'), '0')
    new_t1 = '%B' + padded_phone_num + '^ GARAGE / ILDC^YYMMSSSDDDDDDDDDDDDDDDDDDDDDDDDD?'
    new_t2 = ';' + padded_phone_num + '=YYMMSSSDDDDDDDDDDDDDD?'
    
    #new_t1 = raw_input('Track 1: ')
    #new_t2 = raw_input('Track 2: ')
    assert len(new_t1) <= len(ORIG_T1)
    assert len(new_t2) <= len(ORIG_T2)

    ih = IntelHex()
    ih.loadhex(open(ORIG_BIN, 'r'))
    assert ih.gets(ORIG_T1_OFFSET, len(ORIG_T1)) == ORIG_T1
    assert ih.gets(ORIG_T2_OFFSET, len(ORIG_T2)) == ORIG_T2
    ih.puts(ORIG_T1_OFFSET, new_t1.ljust(len(ORIG_T1), chr(0)))
    ih.puts(ORIG_T2_OFFSET, new_t2.ljust(len(ORIG_T2), chr(0)))
    ih.write_hex_file(open(NEW_BIN, 'w'))

    executable = './avrdude'
    if os.name == 'nt':
        executable = 'avrdude.exe'

    os.system('%s %s -v -pattiny85 -c%s -U lfuse:w:0xe2:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m -U flash:w:%s:i' % (executable, AVRDUDE_PARAMS, PROGRAMMER, NEW_BIN))


if __name__ == '__main__':
    main()
