import os
import sys

SERIAL = '/dev/cu.usbserial-A104OKWO'

ORIG_BIN = 'esp8266_deauther.bin'
NEW_BIN = 'esp8266_deauther.bin.patched'
ORIG_SSID = 'BlueNights_12345'


def main():
    new_ssid = raw_input('Enter SSID: ')
    assert len(new_ssid) > 0
    assert len(new_ssid) <= len(ORIG_SSID)

    data = open(ORIG_BIN, 'rb').read()
    new_data = data.replace(ORIG_SSID, new_ssid.ljust(len(ORIG_SSID), chr(0)))
    open(NEW_BIN, 'wb').write(new_data)

    executable = './esptool'
    if os.name == 'nt':
        executable = 'esptool.exe'

    os.system('%s -vv -cd ck -cb 115200 -cp %s -ca 0x00000 -cf %s' % (executable, SERIAL, NEW_BIN))


if __name__ == "__main__":
    main()
