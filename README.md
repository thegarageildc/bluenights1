# Welcome
This repository contains both code and instructions for two projects: **MagSpoof** and **WiFi Deauther**

## Prerequisites
- [Python 2.7](https://www.python.org/ftp/python/2.7.13/python-2.7.13.msi)
- [Arduino Studio]( )https://www.arduino.cc/en/Main/Donate
- `pip install intelhex`
### Optional
- For SparkFun AVR pocket programmer, install [Zadig Drivers](https://cdn.sparkfun.com/assets/learn_tutorials/2/1/4/zadig_v2.0.1.160.zip)

## MagSpoof
### Prerequisites
- AVR programmer (USBasp, USBtinyISP, AVR ISP, ..)
-- If compiling from source, add support for programming ATtiny-based boards by following the instructions [here](http://highlowtech.org/?p=1695).
### Quick start
- Open `upload.py` and make sure PROGRAMMER is the correct programmer type. Tune AVRDUDE_PARAMS if needed.
- `python upload.py`
### Instructions (compile from source)
- Open `magspoof-arduino` in Arduino Studio.
- In order to edit the magnetic card data:
-- Scroll down to `const char* tracks[] = {`
-- Each line under this represents a track (by default two lines/tracks are present).
- Tools -> Board -> ATtiny25/45/85
- Tools -> Processor -> ATtiny85
- Tools -> Clock -> Internal 8 MHz
- Sketch -> Upload Using Programmer


## WiFi Deauther
### Prerequisites
- 3.3v FTDI
### Quick start
- Open `upload.py` and make sure SERIAL points to your FTDI port
- Boot ESP8266 with boot jumper set
- `python upload.py`
### Instructions (compiling from source)
- Follow instructions [here](https://github.com/spacehuhn/esp8266_deauther)